package employeeinfo;

import java.util.Arrays;

public class AccountList {
    private final int INITIAL_LENGTH = 2;
    private int size;
    private Account[] account;

    public AccountList() {
        account = new Account[INITIAL_LENGTH];
        size = 0;
    }

    public void add(Account s){
        if(s==null){
            return;
        }
        if(size==account.length) {
            this.resize();
        }
        account[size++] = s;
    }

    public Account get(int i){
        return account[i];
    }

    public boolean find(Account s){
        for (Account value : account) {
            if (s.equals(value)) {
                return true;
            }
        }
        return false;
    }

    public void insert(Account s, int pos){
        if(pos> account.length || pos<0)
            return;

        this.resize(); // N => N+1
        size++;
        for(int i=size-1;i>pos;i--)
        {
            account[i]=account[i-1];
        }
        account[pos]=s;
    }

    public boolean remove(Account s){
        if(s==null)
            return false;
        for(int i=0;i<size;i++) {
            if (s.equals(account[i])) {
                for(int j=i;j<size-1;j++){
                    account[j] = account[j + 1];
                }
                size--;
                return true;
            }
        }
        return false;

    }


    private void resize(){
        account= Arrays.copyOf(account,account.length+size);
    }
    public String toString(){
        String str="[";
        for(int i=0;i<size;i++) {
            str+=account[i];
            if(i<size-1)
                str+=",";
        }

        str+="]";
        return str;
    }
    public int size() {
        return this.size;
    }
}
