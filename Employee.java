package employeeinfo;

import java.time.LocalDate;

public class Employee {

	private AccountList accountList;

	private String name;
	private LocalDate hireDate;
	
	public Employee(String name,int yearOfHire, int monthOfHire, int dayOfHire){
		this.name = name;
		hireDate = LocalDate.of(yearOfHire,monthOfHire,dayOfHire);
		accountList = new AccountList();
	}

	
	public void createNewChecking(double startAmount) {
		Account acct = new CheckingAccount(this, startAmount);
		accountList.add(acct);
	}

	public void createNewSavings(double startAmount) {
		Account acct = new SavingsAccount(this, startAmount);
		accountList.add(acct);
	}

	public void createNewRetirement(double startAmount) {
		Account acct = new RetirementAccount(this, startAmount);
		accountList.add(acct);
	}

	public String getFormattedAcctInfo() {
		String output = "";
		for (int i = 0; i < accountList.size(); i++) {
			output += accountList.get(i).toString();
		}
		return "ACCOUNT INFO FOR " + this.getName() + "\n\n" + output + "\n\n";
	}

	public MyArrayList getAccounts(){
		MyArrayList names = new MyArrayList();
		for(int i = 0; i < accountList.size(); ++i){
			names.add(accountList.get(i).getAcctType().toString().toLowerCase());
		}
		return names;

	}

	public void deposit(int accId, double amt){
		Account selected = accountList.get(accId);
		selected.makeDeposit(amt);
	}
	public boolean withdraw(int accId, double amt){
		Account selected = accountList.get(accId);
		return selected.makeWithdrawal(amt);
	}

	private String format = "name = $s, salary = %.2f, hireDay = %s";

	public String toString(){
		return String.format(format,name,hireDate.toString());
	}

	public String getName() {
		return name;
	}

	public LocalDate getHireDate() {
		return hireDate;
	}
}
