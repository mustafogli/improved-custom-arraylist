
import employeeinfo.MyArrayList;
import employeeinfo.Employee;

import java.util.Scanner;

public class Main {
	Employee[] emps = null;
	public static void main(String[] args) {
		new Main();
	}
	Main(){
		emps = new Employee[3];
		emps[0] = new Employee("Jim Daley", 2000, 9, 4);
		emps[1] = new Employee("Bob Reuben", 1998, 1, 5);
		emps[2] = new Employee("Susan Randolph", 1997, 2,13);
		
		emps[0].createNewChecking(10500);
		emps[0].createNewSavings(1000);
		emps[0].createNewRetirement(9300);
		emps[1].createNewChecking(34000);
		emps[1].createNewSavings(27000);
		emps[2].createNewChecking(10038);
		emps[2].createNewSavings(12600);
		emps[2].createNewRetirement(9000);	
		
				
		Scanner sc = new Scanner(System.in);
		System.out.print("See a report of all account balances? (y/n) ");
		String answer = sc.next();
		boolean isDone = false;

		while (!isDone) {
			if (answer.equalsIgnoreCase("y")) {
				System.out.println("A. See a report of all accounts.");
				System.out.println("B. Make a deposit.");
				System.out.println("C. Make a withdrawal.");
				System.out.print("Make a selection (A/B/C): ");
				String selection = sc.next().toUpperCase();

				if (selection.equals("A")) {
					String result = "";
					for (Employee emp : emps) {
						result += emp.getFormattedAcctInfo();
					}
					System.out.println(result);

				}

				if (selection.equals("B") || selection.equals("C")) {

					int getEmployee = this.getEmployee();
					String[] getEmployeeAccount = this.getEmployeeAccount(getEmployee);

					if (selection.equals("B")) {
						System.out.print("Deposit amount: ");
						double amt = sc.nextDouble();

						emps[getEmployee].deposit(Integer.parseInt(getEmployeeAccount[0]), amt);
						System.out.println("$" + amt + " has been deposited in the " + getEmployeeAccount[1]
								+ " account of " + emps[getEmployee].getName());
					}

					if (selection.equals("C")) {
						System.out.print("Withdraw amount: ");
						double amt = sc.nextDouble();

						emps[getEmployee].deposit(Integer.parseInt(getEmployeeAccount[0]), amt);
						System.out.println("$" + amt + " has been deposited in the " + getEmployeeAccount[1]
								+ " account of " + emps[getEmployee].getName());
					}

				}

				System.out.print("Peform transaction: (y/n): ");
				answer = sc.next();
			} else {
				isDone = true;
			}
		}
	}
	public int getEmployee() {
		for (int i = 0; i < emps.length; i++) {
			System.out.println(i + ". " + emps[i].getName());
		}
		Scanner sc = new Scanner(System.in);
		System.out.print("Select an employee: (type a number) ");
		int employeeIndex = sc.nextInt();

		return employeeIndex;
	}

	public String[] getEmployeeAccount(int employeeId) {
		MyArrayList accountTypes = emps[employeeId].getAccounts();

		for (int i = 0; i < accountTypes.size(); i++) {
			System.out.println(i + ": " + accountTypes.get(i));
		}
		Scanner sc = new Scanner(System.in);
		System.out.print("Select an account: (type a number) ");
		int accountIndex = sc.nextInt();

		return new String[] { String.valueOf(accountIndex), accountTypes.get(accountIndex) };
	}
}
